﻿using MarioEngine.API;
using MarioEngine.API.Attributes;
using MarioEngine.API.Services.Tiles;
using MarioEngine.API.TileHandlers;
using MarioEngine.API.Tiles;
using MarioEngine.DefaultContent.Constants;
using MarioEngine.DefaultContent.IoC;
using MarioEngine.DefaultContent.TileHandlers;
using MarioEngine.DefaultContent.Tiles;
using Ninject;

namespace MarioEngine.DefaultContent
{
	[ Plugin ]
	public class DefaultContentProvider : IContentProvider
	{
		private readonly ITileRegistry       tileRegistry;
		private readonly ITileHandlerFactory tileHandlerFactory;

		public DefaultContentProvider( ITileRegistry tileRegistry,
									   ITileHandlerFactory tileHandlerFactory )
		{
			this.tileRegistry       = tileRegistry;
			this.tileHandlerFactory = tileHandlerFactory;
		}

		public string Name => "Default Content";

		public void RegisterModules( IKernel kernel )
		{
			kernel.Load<DefaultContentModule>();
		}

		public void LoadContent()
		{
			BasicTileHandler basicTileHandler = this.tileHandlerFactory.Create<BasicTileHandler>();

			basicTileHandler.AddType<TileBase>( TileNames.StandardTerrain );
			basicTileHandler.AddType<TileQBlock>( TileNames.QuestionBlock );

			this.tileRegistry.RegisterTileHandler( basicTileHandler );
			this.tileRegistry.RegisterTileHandler<TileGroupHandler>();
		}
	}
}