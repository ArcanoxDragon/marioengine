﻿namespace MarioEngine.DefaultContent.Constants
{
	public static class TileNames
	{
		public const string StandardTerrain = "stdTerrain";
		public const string QuestionBlock   = "qBlock";
		public const string TileGroup       = "tileGroup";
	}
}