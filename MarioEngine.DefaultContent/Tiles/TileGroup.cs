﻿using System;
using System.IO;
using MarioEngine.API.Constants;
using MarioEngine.API.Services.Tiles;
using MarioEngine.API.Tiles;

namespace MarioEngine.DefaultContent.Tiles
{
	public class TileGroup : TileBase
	{
		private readonly ITileRegistry registry;

		private int width;
		private int height;

		public TileGroup( ITileRegistry registry, string tileTypeName ) : base( tileTypeName )
		{
			this.registry = registry;
		}

		public override int Width  => this.width;
		public override int Height => this.height;

		internal string ChildType { get; set; }
		internal byte[] ChildData { get; set; }

		internal void SetSize( int width, int height )
		{
			this.width  = width;
			this.height = height;
		}

		protected override void OnAddedToLevel()
		{
			// Split combined tiles into individual ones as long as we're not in the editor
			if ( this.Game.CurrentState.Name != StateNames.Editor )
			{
				if ( this.Width == 0 || this.Height == 0 )
				{
					// We're an empty group; just remove ourself

					this.Level.RemoveTile( this.Layer, this );
				}
				else
				{
					// Convert into our proper child tiles

					using ( var childDataStream = new MemoryStream( this.ChildData ) )
					using ( var childReader = new BinaryReader( childDataStream ) )
					{
						for ( var offsetX = 0; offsetX < this.Width; offsetX++ )
						for ( var offsetY = 0; offsetY < this.Height; offsetY++ )
						{
							var childTile = this.registry.ReadTile( childReader, this.ChildType );

							childTile.X = this.X + offsetX * childTile.Width;
							childTile.Y = this.Y + offsetY * childTile.Height;

							this.Level.AddTile( this.Layer, childTile );

							// Rewind the stream
							childDataStream.Seek( 0, SeekOrigin.Begin );
						}
					}

					this.Level.RemoveTile( this.Layer, this );
				}
			}
		}

		public override void Update( TimeSpan tickLength ) { }
	}
}