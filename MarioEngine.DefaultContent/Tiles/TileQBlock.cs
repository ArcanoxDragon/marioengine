﻿using System;
using GameDotNet.Graphics;
using GameDotNet.Utility.Graphics;
using MarioEngine.API.Tiles;
using OpenTK.Graphics;

namespace MarioEngine.DefaultContent.Tiles
{
	class TileQBlock : TileBase
	{
		private const float AnimationDelta = (float) Math.PI / 8.0f;

		private Model            model;
		private TextureAnimation animation;
		private float            bounceProgress;
		private bool             isBouncing;

		public TileQBlock( string tileTypeName ) : base( tileTypeName ) { }

		public override int Width  => 1;
		public override int Height => 1;

		public override void Update( TimeSpan deltaTime )
		{
			if ( this.isBouncing )
			{
				this.bounceProgress += AnimationDelta;

				if ( this.bounceProgress >= Math.PI )
				{
					this.bounceProgress = 0.0f;
					this.isBouncing     = false;
				}
			}

			if ( this.Level.Random.Next( 100 ) == 0 )
				this.isBouncing = true;
		}

		public override void Render( TimeSpan deltaTime )
		{
			if ( this.model == null || this.animation == null )
			{
				this.model     = this.Game.Models.Get( "Level/QBlock.obj" );
				this.animation = this.Game.Animations.GetOrCreate( this.model.DiffuseMap );
			}

			var animScale  = (float) Math.Sin( this.bounceProgress ) * 0.05f + 1.0f;
			var animXShift = (float) Math.Sin( this.bounceProgress ) * -0.05f;
			var animYShift = (float) Math.Sin( this.bounceProgress ) * 0.3f;
			var animZShift = (float) Math.Sin( this.bounceProgress ) * 0.1f;

			this.Transform.PushMatrix();
			{
				this.Transform.Translate( -this.X, -this.Y, 0 );
				this.Transform.Scale( animScale, animScale, 1.0f );
				this.Transform.Translate( this.X + animXShift, this.Y + animYShift, animZShift );
				this.Game.Shaders.Current.SetUniform( "tintColor", Color4.White );
				this.Transform.UVPushMatrix();
				{
					this.animation.TransformUV();
					this.model.RenderModel();
				}
				this.Transform.UVPopMatrix();
			}
			this.Transform.PopMatrix();
		}
	}
}