﻿using System;
using System.IO;
using MarioEngine.API.Services.Tiles;
using MarioEngine.API.TileHandlers;
using MarioEngine.API.Tiles;
using MarioEngine.DefaultContent.Constants;
using MarioEngine.DefaultContent.Tiles;

namespace MarioEngine.DefaultContent.TileHandlers
{
	public class TileGroupHandler : ITileHandler
	{
		private readonly BasicTileHandler basicHandler;

		public TileGroupHandler( ITileHandlerFactory tileHandlerFactory )
		{
			this.basicHandler = tileHandlerFactory.Create<BasicTileHandler>();
			this.ConfigureBasicHandler();
		}

		private void ConfigureBasicHandler()
		{
			this.basicHandler.AddType<TileGroup>( TileNames.TileGroup );
		}

		public bool HandlesType( string typeName ) => this.basicHandler.HandlesType( typeName );

		public void WriteTile( BinaryWriter writer, ITile tile )
		{
			if ( !( tile is TileGroup tileGroup ) )
				throw new NotSupportedException( $"{nameof(TileGroupHandler)} only supports tiles of type {typeof( TileGroup ).FullName}" );

			this.basicHandler.WriteTile( writer, tile );

			writer.Write( tileGroup.Width );
			writer.Write( tileGroup.Height );
			writer.Write( tileGroup.ChildType );
			writer.Write( tileGroup.ChildData.Length );
			writer.Write( tileGroup.ChildData );
		}

		public ITile ReadTile( BinaryReader reader, string tileTypeName )
		{
			var newTile = this.basicHandler.ReadTile( reader, tileTypeName );

			if ( !( newTile is TileGroup tileGroup ) )
				throw new InvalidOperationException( $"{nameof(TileGroup)} could not be read from the stream" );

			var groupWidth      = reader.ReadInt32();
			var groupHeight     = reader.ReadInt32();
			var childType       = reader.ReadString();
			var childDataLength = reader.ReadInt32();
			var childData       = reader.ReadBytes( childDataLength );

			tileGroup.SetSize( groupWidth, groupHeight );
			tileGroup.ChildType = childType;
			tileGroup.ChildData = childData;

			return tileGroup;
		}
	}
}