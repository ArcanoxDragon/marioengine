﻿using GameDotNet.Utility.Graphics;
using MarioEngine.API.Constants;
using OpenTK.Graphics;

namespace MarioEngine.Gui
{
	class Button : Control
	{
		public delegate void ButtonAction( Button sender );

		public event ButtonAction OnPressed;

		public Button( GuiScreen owner ) : base( owner )
		{
			this.Text               = "";
			this.ShowSelectionArrow = true;
		}

		public string Text               { get; set; }
		public bool   ShowSelectionArrow { get; set; }

		public override void OnPressRight() { }

		public override void OnPressLeft() { }

		public override void OnPressEnter()
		{
			this.OnPressed?.Invoke( this );
		}

		public override void OnUpdate( bool selected ) { }

		public override void OnRender( bool selected )
		{
			var game            = this.Owner.Game;
			var textures        = game.Textures.GetLibrary();
			var menuFont        = game.Fonts.Get( "Menu" );
			var normalTexture   = textures.GetTexture( "Menu/Button" );
			var selectedTexture = textures.GetTexture( "Menu/SelectedButton" );

			var centerX   = this.Location.X + this.Size.Width / 2;
			var centerY   = this.Location.Y + this.Size.Height / 2;
			var textX     = centerX - menuFont.GetStringWidth( this.Text ) / 2;
			var textY     = centerY - menuFont.GetStringHeight( this.Text ) / 2;
			var tintColor = selected ? GuiConstants.Buttons.SelectedTintColor : GuiConstants.Buttons.NormalTintColor;

			game.Shaders.Current.SetUniform( "tintColor", Color4.White );
			normalTexture.Bind();
			game.Gl.RenderBorderedPlane( this.X, this.Y, this.Width, this.Height, normalTexture.Width / 3 );

			if ( selected && this.ShowSelectionArrow )
			{
				selectedTexture.Bind();
				game.Gl.RenderPlane( this.X + 8, textY, selectedTexture.Width, selectedTexture.Height );
			}

			game.Shaders.Current.SetUniform( "tintColor", tintColor );
			menuFont.RenderString( this.Text, textX, textY );
		}
	}
}