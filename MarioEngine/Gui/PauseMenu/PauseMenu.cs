﻿using System;
using System.Linq;
using GameDotNet.Graphics;
using GameDotNet.Utility.Graphics;
using MarioEngine.API;
using MarioEngine.Engine.States;
using OpenTK.Graphics;
using OpenTK.Input;

namespace MarioEngine.Gui.PauseMenu
{
	public class PauseMenu
	{
		private Texture backgroundTexture;
		private Texture selectedTexture;
		private Font    fontNormal;
		private Font    fontLarge;

		public PauseMenu( StateWithPauseMenu state )
		{
			this.State   = state;
			this.Actions = state.GetPauseMenuActions().ToArray();
		}

		public int SelectedButton { get; set; }

		protected StateWithPauseMenu State   { get; }
		protected PauseMenuAction[]  Actions { get; }

		protected IMarioGame Game => this.State.Game;

		public void Update( TimeSpan deltaTime )
		{
			if ( this.Game.KeyboardState[ Key.Up ] && !this.Game.LastKeyboardState[ Key.Up ] )
			{
				if ( --this.SelectedButton < 0 )
					this.SelectedButton = this.Actions.Length - 1;
			}

			if ( this.Game.KeyboardState[ Key.Down ] && !this.Game.LastKeyboardState[ Key.Down ] )
			{
				if ( ++this.SelectedButton >= this.Actions.Length )
					this.SelectedButton = 0;
			}

			if ( this.Game.LastKeyboardState[ Key.Enter ] && !this.Game.KeyboardState[ Key.Enter ] )
			{
				if ( this.SelectedButton >= 0 && this.SelectedButton < this.Actions.Length )
					this.Actions[ this.SelectedButton ].Action();
			}
		}

		public void Render( TimeSpan deltaTime )
		{
			var textures = this.Game.Textures.GetLibrary();

			if ( this.backgroundTexture == null )
				this.backgroundTexture = textures.GetTexture( "Menu/PauseBG" );
			if ( this.selectedTexture == null )
				this.selectedTexture = textures.GetTexture( "Menu/SelectedButton" );
			if ( this.fontNormal == null )
				this.fontNormal = this.Game.Fonts.Get( "Menu" );
			if ( this.fontLarge == null )
				this.fontLarge = this.Game.Fonts.Get( "MenuBig" );

			var width       = this.Game.Window.Width;
			var height      = this.Game.Window.Height;
			var backgroundX = width / 2 - this.backgroundTexture.Width / 2;
			var backgroundY = height / 2 - this.backgroundTexture.Height / 2;
			var textX       = width / 2 - this.fontLarge.GetStringWidth( this.State.PauseTitle ) / 2;

			this.Game.Transform.PushMatrix();
			{
				this.Game.Transform.Scale( this.backgroundTexture.Width, this.backgroundTexture.Height, 1.0f );
				this.Game.Transform.Translate( backgroundX, backgroundY, 0.0f );
				this.backgroundTexture.Bind();
				this.Game.Shaders.Current.SetUniform( "tintColor", Color4.White );
				this.Game.Shapes.Plane.Render();
			}
			this.Game.Transform.PopMatrix();

			this.fontLarge.RenderString( this.State.PauseTitle, textX, backgroundY + 8 );

			for ( var action = 0; action < this.Actions.Length; action++ )
			{
				this.Game.Shaders.Current.SetUniform( "tintColor", action == this.SelectedButton ? Color4.Gold : Color4.White );

				textX = width / 2 - this.fontNormal.GetStringWidth( this.Actions[ action ].Text ) / 2;

				var buttonShift = 0;

				if ( this.Actions.Length > 2 )
					buttonShift -= ( this.Actions.Length - 2 ) * 24;

				this.fontNormal.RenderString( this.Actions[ action ].Text, textX, backgroundY + 64 + action * 24 + buttonShift );

				if ( action == this.SelectedButton )
				{
					this.Game.Transform.PushMatrix();
					{
						this.Game.Transform.Scale( this.selectedTexture.Width, this.selectedTexture.Height, 1.0f );
						this.Game.Transform.Translate( textX - this.selectedTexture.Width - 8, backgroundY + 64 + action * 24 + buttonShift, 0.0f );
						this.selectedTexture.Bind();
						this.Game.Shaders.Current.SetUniform( "tintColor", Color4.White );
						this.Game.Shapes.Plane.Render();
					}
					this.Game.Transform.PopMatrix();
				}
			}
		}
	}
}