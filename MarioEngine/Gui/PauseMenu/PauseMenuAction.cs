﻿using System;

namespace MarioEngine.Gui.PauseMenu
{
	public class PauseMenuAction
	{
		public PauseMenuAction( string text, Action action )
		{
			this.Text   = text;
			this.Action = action;
		}

		public string Text   { get; set; }
		public Action Action { get; set; }
	}
}