﻿using System.Collections.Generic;
using GameDotNet.Graphics;
using MarioEngine.API;
using OpenTK.Input;

namespace MarioEngine.Gui
{
	public class GuiScreen
	{
		private int    selectedControl;
		private int    mouseX;
		private int    mouseY;
		private Shader menuShader;

		public GuiScreen( MarioState state )
		{
			this.State    = state;
			this.Controls = new List<Control>();

			this.Game.Window.MouseMove += this.OnWindowMouseMove;
		}

		public MarioState    State    { get; }
		public List<Control> Controls { get; }

		public IMarioGame Game => this.State.Game;

		private void OnWindowMouseMove( object sender, MouseMoveEventArgs e )
		{
			this.mouseX = e.X;
			this.mouseY = e.Y;
		}

		public void AddControl( Control control )
		{
			this.Controls.Add( control );
		}

		public virtual void UpdateScreen()
		{
			#region Left

			if ( this.Game.GetKeyOneShot( Key.Left ) || this.Game.GetKeyOneShot( Key.A ) )
			{
				if ( this.Controls.Count > 0 && this.selectedControl >= 0 && this.selectedControl < this.Controls.Count )
				{
					this.Controls[ this.selectedControl ].OnPressLeft();
				}
			}

			#endregion Left

			#region Right

			if ( this.Game.GetKeyOneShot( Key.Right ) || this.Game.GetKeyOneShot( Key.D ) )
			{
				if ( this.Controls.Count > 0 && this.selectedControl >= 0 && this.selectedControl < this.Controls.Count )
				{
					this.Controls[ this.selectedControl ].OnPressRight();
				}
			}

			#endregion Right

			#region Enter

			if ( this.Game.GetKeyOneShot( Key.Enter, true ) || this.Game.GetKeyOneShot( Key.Space, true ) )
			{
				if ( this.Controls.Count > 0 && this.selectedControl >= 0 && this.selectedControl < this.Controls.Count )
				{
					this.Controls[ this.selectedControl ].OnPressEnter();
				}
			}

			#endregion Enter

			#region Up

			if ( this.Game.GetKeyOneShot( Key.Up ) || this.Game.GetKeyOneShot( Key.W ) )
			{
				if ( this.Controls.Count > 0 )
				{
					this.selectedControl--;
					if ( this.selectedControl < 0 )
						this.selectedControl = this.Controls.Count - 1;
				}
			}

			#endregion Up

			#region Down

			if ( this.Game.GetKeyOneShot( Key.Down ) || this.Game.GetKeyOneShot( Key.S ) )
			{
				if ( this.Controls.Count > 0 )
				{
					this.selectedControl = ( this.selectedControl + 1 ) % this.Controls.Count;
				}
			}

			#endregion Down

			for ( int control = 0; control < this.Controls.Count; control++ )
			{
				var controlX      = this.Controls[ control ].Location.X;
				var controlY      = this.Controls[ control ].Location.Y;
				var controlWidth  = this.Controls[ control ].Size.Width;
				var controlHeight = this.Controls[ control ].Size.Height;

				if ( this.mouseX >= controlX &&
					 this.mouseY >= controlY &&
					 this.mouseX <= controlX + controlWidth &&
					 this.mouseY <= controlY + controlHeight )
				{
					this.selectedControl = control;

					if ( this.Game.GetMouseOneShot( MouseButton.Left ) )
					{
						this.Controls[ control ].OnPressEnter();
					}
				}
			}

			for ( int control = 0; control < this.Controls.Count; control++ )
			{
				this.Controls[ control ].OnUpdate( this.selectedControl == control );
			}
		}

		public virtual void RenderScreen()
		{
			if ( this.menuShader == null )
				this.menuShader = this.Game.Shaders.Get( "gui" );

			this.menuShader.Bind();

			for ( int control = 0; control < this.Controls.Count; control++ )
			{
				this.Controls[ control ].OnRender( this.selectedControl == control );
			}
		}
	}
}