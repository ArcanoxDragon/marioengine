﻿using System.Drawing;

namespace MarioEngine.Gui
{
	public abstract class Control
	{
		protected Control( GuiScreen owner )
		{
			this.Owner = owner;
		}

		public GuiScreen Owner { get; }

		public virtual Point Location { get; set; }
		public virtual Size  Size     { get; set; }

		public int X
		{
			get => this.Location.X;
			set => this.Location = new Point( value, this.Location.Y );
		}

		public int Y
		{
			get => this.Location.Y;
			set => this.Location = new Point( this.Location.X, value );
		}

		public int Width
		{
			get => this.Size.Width;
			set => this.Size = new Size( value, this.Size.Height );
		}

		public int Height
		{
			get => this.Size.Height;
			set => this.Size = new Size( this.Size.Width, value );
		}

		public abstract void OnPressRight();
		public abstract void OnPressLeft();
		public abstract void OnPressEnter();

		public abstract void OnUpdate( bool selected );
		public abstract void OnRender( bool selected );
	}
}