﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MarioEngine.API.Constants;
using OpenTK.Input;

namespace MarioEngine.Gui
{
	class ControlRow : Control
	{
		private int  selectedControl;
		private int  mouseX;
		private int  mouseY;
		private bool isSelected;

		public ControlRow( GuiScreen owner ) : base( owner )
		{
			this.Controls = new List<Control>();

			owner.Game.Window.MouseMove += this.OnWindowMouseMove;
		}

		public List<Control> Controls { get; }

		private void OnWindowMouseMove( object sender, MouseMoveEventArgs e )
		{
			this.mouseX = e.X;
			this.mouseY = e.Y;
		}

		public override Point Location
		{
			get => base.Location;
			set
			{
				base.Location = value;

				if ( this.Controls != null )
				{
					int curWidth = 0;

					foreach ( var control in this.Controls )
					{
						control.X =  this.X + curWidth;
						control.Y =  this.Y;
						curWidth  += control.Width + GuiConstants.ControlSpacing;
					}
				}
			}
		}

		public override Size Size
		{
			get
			{
				int maxHeight  = 0;
				int totalWidth = 0;

				for ( var control = 0; control < this.Controls.Count; control++ )
				{
					if ( this.Controls[ control ].Height > maxHeight )
						maxHeight = this.Controls[ control ].Height;

					totalWidth += this.Controls[ control ].Width;

					if ( control < this.Controls.Count - 1 )
						totalWidth += GuiConstants.ControlSpacing;
				}

				return new Size( totalWidth, maxHeight );
			}
			set => throw new InvalidOperationException( "Cannot resize a control group" );
		}

		public void AddControl( Control control )
		{
			this.Controls.Add( control );
		}

		public override void OnPressLeft()
		{
			if ( !this.isSelected )
				return;

			if ( this.Controls.Count > 0 )
			{
				if ( --this.selectedControl < 0 )
					this.selectedControl = this.Controls.Count - 1;
			}
		}

		public override void OnPressRight()
		{
			if ( !this.isSelected )
				return;

			if ( this.Controls.Count > 0 )
				this.selectedControl = ( this.selectedControl + 1 ) % this.Controls.Count;
		}

		public override void OnPressEnter()
		{
			if ( !this.isSelected )
				return;

			if ( this.Controls.Any() && this.selectedControl >= 0 && this.selectedControl < this.Controls.Count )
				this.Controls[ this.selectedControl ].OnPressEnter();
		}

		public override void OnUpdate( bool selected )
		{
			this.isSelected = selected;

			if ( selected )
			{
				for ( var control = 0; control < this.Controls.Count; control++ )
				{
					int controlX      = this.Controls[ control ].Location.X;
					int controlY      = this.Controls[ control ].Location.Y;
					int controlWidth  = this.Controls[ control ].Size.Width;
					int controlHeight = this.Controls[ control ].Size.Height;

					if ( this.mouseX >= controlX &&
						 this.mouseY >= controlY &&
						 this.mouseX <= controlX + controlWidth &&
						 this.mouseY <= controlY + controlHeight )
					{
						this.selectedControl = control;
					}
				}
			}

			for ( int control = 0; control < this.Controls.Count; control++ )
			{
				this.Controls[ control ].OnUpdate( this.selectedControl == control && selected );
			}
		}

		public override void OnRender( bool selected )
		{
			for ( int control = 0; control < this.Controls.Count; control++ )
			{
				this.Controls[ control ].OnRender( this.selectedControl == control && selected );
			}
		}
	}
}