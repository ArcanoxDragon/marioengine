﻿using System.IO;

namespace MarioEngine.Exceptions
{
	sealed class LevelLoadException : FileLoadException // Just for instance checking and more specific errors
	{
		public LevelLoadException( string message ) : base( message ) { }
	}
}