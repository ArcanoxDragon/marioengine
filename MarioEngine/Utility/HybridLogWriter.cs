﻿using System.IO;
using System.Text;

namespace MarioEngine.Utility
{
	sealed class HybridLogWriter : TextWriter
	{
		private readonly TextWriter   parent;
		private readonly StreamWriter logWriter;

		public HybridLogWriter( TextWriter parent, string logFilename )
		{
			this.parent = parent;

			if ( File.Exists( logFilename ) )
				File.Delete( logFilename );

			this.logWriter = new StreamWriter( logFilename );
		}

		public override Encoding Encoding => this.parent.Encoding;

		public override void Write( char value )
		{
			this.parent.Write( value );
			this.logWriter.Write( value );

			if ( value == '\n' )
				this.logWriter.Flush();
		}
	}
}