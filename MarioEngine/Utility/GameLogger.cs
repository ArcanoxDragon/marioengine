﻿using System;
using System.Diagnostics;
using GameDotNet.Abstract.Services;

namespace MarioEngine.Utility
{
	public class GameLogWriter : ILogWriter
	{
		public void Write( string text )
		{
			Console.Write( text );
			Debug.Write( text );
		}

		public void Write( string format, params object[] args )
		{
			Console.Write( format, args );
			Debug.Write( string.Format( format, args ) );
		}

		public void WriteLine( string text )
		{
			Console.WriteLine( text );
			Debug.WriteLine( text );
		}

		public void WriteLine( string format, params object[] args )
		{
			Console.WriteLine( format, args );
			Debug.WriteLine( format, args );
		}
	}
}