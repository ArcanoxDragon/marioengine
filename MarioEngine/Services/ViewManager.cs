﻿using GameDotNet.Extensions;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.API.Services;
using OpenTK;

namespace MarioEngine.Services
{
	class ViewManager : IViewManager
	{
		private readonly IGameAccessor gameAccessor;

		public ViewManager( IGameAccessor gameAccessor )
		{
			this.gameAccessor = gameAccessor;
		}

		public IMarioGame Game => this.gameAccessor.Game;

		public void SetProjectionForGui()
		{
			var scaleX = 1f / this.Game.Window.Width;
			var scaleY = 1f / this.Game.Window.Height;
			var projection = Matrix4.Identity
									.Scale( scaleX, scaleY, 1f )
									.Scale( 2f,     -2f,    1f )
									.Translate( -1f, 1f, 0f );

			this.Game.Transform.SetProjection( projection );
			this.Game.Transform.SetView( Matrix4.Identity );
		}

		public void SetProjectionForLevel( float cameraX = 0f, float cameraY = 0f, float viewHeight = 1f )
		{
			var aspectRatio = this.Game.Window.Width / (float) this.Game.Window.Height;
			var viewWidth   = aspectRatio * viewHeight;
			var projection  = Matrix4.CreateOrthographic( viewWidth, viewHeight, 0.01f, 100f );

			this.Game.Transform.SetProjection( projection );

			var viewHalfWidth  = viewWidth / 2f;
			var viewHalfHeight = viewHeight / 2f;
			var view = Matrix4.LookAt( viewHalfWidth, viewHalfHeight, 1.0f,
									   viewHalfWidth, viewHalfHeight, 0.0f,
									   0.0f, 1.0f, 0.0f )
							  .Translate( -cameraX, -cameraY, 0.0f );

			this.Game.Transform.SetView( view );
		}
	}
}