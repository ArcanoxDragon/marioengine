﻿using System;
using MarioEngine.API;
using MarioEngine.API.Services;

namespace MarioEngine.Services
{
	public class GameAccessor : IGameAccessor
	{
		private readonly Func<IMarioGame> gameAccessorFunc;

		public GameAccessor( Func<IMarioGame> gameAccessorFunc )
		{
			this.gameAccessorFunc = gameAccessorFunc;
		}

		public IMarioGame Game => this.gameAccessorFunc();
	}
}