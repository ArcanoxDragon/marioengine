﻿using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Engine;
using GameDotNet.Extensions;
using GameDotNet.IoC;
using MarioEngine.Config;
using MarioEngine.Engine;
using MarioEngine.IoC;
using MarioEngine.Utility;
using Ninject;

namespace MarioEngine
{
	class Program
	{
		private static GameRunner<MarioGame> GameRunner;
		private static IKernel               Kernel;

		private static void Main( string[] args )
		{
			// Initialize file logging
			HybridLogWriter writer = new HybridLogWriter( Console.Out, "MarioEngine.log" );
			Console.SetOut( writer );

#if !DEBUG
			AppDomain.CurrentDomain.UnhandledException += UnhandledException;
#endif

			// Load game configuration
			GameConfig.LoadConfig();

			// Set up DI kernel
			Kernel = new StandardKernel( new GameModule(), new MarioGameModule( () => GameRunner.Game ) );

			// Initialize game runner
			GameRunner = Kernel.Get<IGameRunnerFactory>().Create<MarioGame>();
			GameRunner.OnException += GameRunner_OnException;

			// Register all plugin modules
			GameRunner.Game.RegisterIoCModules( Kernel );

			// Finally start the game
			GameRunner.Run();
		}

		private static void GameRunner_OnException( Exception ex )
		{
			Console.WriteLine( "An exception was thrown by the game runner!\n\n" );
			Console.WriteLine( ex );
		}

		private static void UnhandledException( object sender, UnhandledExceptionEventArgs e )
		{
			var logWriter = new HybridLogWriter( Console.Out, "" );

			if ( !( e.ExceptionObject is Exception ) )
			{
				logWriter.WriteLine( "A fatal runtime error has occurred and the game will exit immediately!" );
				Environment.Exit( 1 );
			}

			GameRunner?.Exit();

			logWriter.WriteLine( $"\n{"#".Repeat( 12 )}\n\n" +
								 $"An exception has occurred and the game needs to close:\n{e.ExceptionObject}\n\n" +
								 $"{"#".Repeat( 12 )}\n\n" +
								 "Press any key to continue...\n" );

			Console.ReadKey();
			Environment.Exit( 1 );
		}
	}
}