﻿using System;
using System.Collections.Generic;
using GameDotNet.Graphics;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.Engine;
using MarioEngine.Engine.LevelData;
using MarioEngine.Engine.States;
using MarioEngine.Gui.PauseMenu;
using MarioEngine.Menu;
using Ninject;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace MarioEngine.Gameplay.States
{
	class StateLevel : StateWithPauseMenu
	{
		public StateLevel(
			IMarioGame game,
			[ Named( "Compound" ) ] ILevelFileReader levelReader
		) : base( game )
		{
			this.LevelReader = levelReader;
			this.NeedsResize = true;
		}

		public override string Name           => StateNames.Level;
		public override string SoundtrackName => "Overworld/SMW_1";

		public    ILevelFileReader LevelReader { get; }
		protected Level            Level       { get; set; }
		protected bool             NeedsResize { get; set; }
		protected Shader           LevelShader { get; set; }

		protected override void OnEntered()
		{
			this.Game.Window.Resize += this.Window_OnResize;

			this.LoadLevel();
		}

		private void LoadLevel()
		{
			this.Level = this.LevelReader.LoadLevel( "Levels/Level.json" );

			if ( this.Level == null )
			{
				this.Game.Logger.WriteLine( "Unable to load game level!" );
				this.Game.SwitchState<StateMenu>();
			}
		}

		protected override void OnLeaving()
		{
			this.MusicSource?.Stop();
			this.EffectSource?.Stop();
			this.Music?.Stop();

			if ( this.LevelShader != null )
				this.Game.Shaders.UnloadShader( this.LevelShader );
		}

		private void Window_OnResize( object sender, EventArgs e )
		{
			this.NeedsResize = true;
		}

		protected override void UpdateWhileNotPaused( TimeSpan deltaTime )
		{
			if ( this.Level != null )
			{
				if ( this.Game.MouseState[ MouseButton.Right ] )
				{
					this.Level.CameraX += ( this.Game.MouseState.X - this.Game.LastMouseState.X ) / (float) TileConstants.StandardSize;
					this.Level.CameraY -= ( this.Game.MouseState.Y - this.Game.LastMouseState.Y ) / (float) TileConstants.StandardSize;
				}

				if ( this.Game.KeyboardState[ Key.PageDown ] )
					this.Level.ViewHeight -= 0.5f;

				if ( this.Game.KeyboardState[ Key.PageUp ] )
					this.Level.ViewHeight += 0.5f;

				this.Level.ViewHeight = Math.Max( this.Level.ViewHeight, 10 );

				this.Level.Update( deltaTime );

				this.Game.Title = $"Camera: ({this.Level.CameraX:F2}, {this.Level.CameraY:F2}) | View Height: {this.Level.ViewHeight}";
			}
		}

		protected override void UnloadAllResources()
		{
			base.UnloadAllResources();

			this.LevelShader = null;
			this.LoadLevel();
		}

		protected override void InitializeRenderResourcesIfNeeded()
		{
			base.InitializeRenderResourcesIfNeeded();

			if ( this.LevelShader == null )
				this.LevelShader = this.Game.Shaders.Get( "level" );
		}

		protected override void RenderAlways( TimeSpan deltaTime )
		{
			if ( this.NeedsResize )
			{
				this.NeedsResize = false;
				GL.Viewport( 0, 0, this.Game.Window.Width, this.Game.Window.Height );
			}

			GL.ClearColor( Color4.DeepSkyBlue );
			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );

			#region Level

			this.LevelShader.Bind();
			GL.Enable( EnableCap.DepthTest );
			this.Level?.Render( deltaTime );

			#endregion
		}

		#region Pause Menu

		public override string PauseTitle => "Game Paused";

		public override IEnumerable<PauseMenuAction> GetPauseMenuActions()
		{
			yield return new PauseMenuAction( "Continue",     this.UnpauseGame );
			yield return new PauseMenuAction( "Exit To Menu", () => this.Game.SwitchState<StateMenu>() );
		}

		#endregion
	}
}