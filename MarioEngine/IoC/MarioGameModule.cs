﻿using System;
using GameDotNet.Abstract.Services;
using GameDotNet.Abstract.Services.Resources;
using GameDotNet.Extensions;
using GameDotNet.Services.Resources;
using MarioEngine.Abstract.Services;
using MarioEngine.API;
using MarioEngine.API.Services;
using MarioEngine.API.Services.Tiles;
using MarioEngine.Engine;
using MarioEngine.Engine.LevelData;
using MarioEngine.Engine.LevelData.Binary;
using MarioEngine.Engine.LevelData.Json;
using MarioEngine.Engine.Plugins;
using MarioEngine.Services;
using MarioEngine.Utility;
using Ninject.Extensions.Factory;
using Ninject.Modules;

namespace MarioEngine.IoC
{
	class MarioGameModule : NinjectModule
	{
		private readonly Func<IMarioGame> gameAccessorFunc;

		public MarioGameModule( Func<IMarioGame> gameAccessorFunc )
		{
			this.gameAccessorFunc = gameAccessorFunc;
		}

		public override void Load()
		{
			// First configure options
			this.ConfigureAllOptions( options => {
				// Game options
				options.Game.GameName = MarioGame.GameName;

				// Filesystem options
				options.FileSystemResource.BaseDirectory = "Resources";
			} );

			// Override services in game engine module
			this.Rebind<ILogWriter>().To<GameLogWriter>().InSingletonScope();
			this.Rebind<IResourceProvider>().To<FileSystemResourceProvider>().InSingletonScope();

			// Add our own services
			this.Bind<IGameAccessor>().ToConstant( new GameAccessor( this.gameAccessorFunc ) );
			this.Bind<IContentProviderFactory>().ToFactory( () => new GenericOrTypeParameterInstanceProvider() );
			this.Bind<ILevelFactory>().ToFactory();
			this.Bind<ILevelFileReader>().To<BinaryLevelFileReader>().Named( "Binary" );
			this.Bind<ILevelFileReader>().To<JsonLevelFileReader>().Named( "Json" );
			this.Bind<ILevelFileReader>().To<CompoundLevelFileReader>().Named( "Compound" );
			this.Bind<ILevelFileWriter>().To<BinaryLevelFileWriter>().Named( "Binary" );
			this.Bind<ITileHandlerFactory>().ToFactory();
			this.Bind<IBasicTileFactory>().ToFactory( () => new GenericOrTypeParameterInstanceProvider() );
			this.Bind<ITileRegistry>().To<TileRegistry>().InSingletonScope();
			this.Bind<PluginLoader>().ToSelf().InSingletonScope();
			this.Bind<IViewManager>().To<ViewManager>();
		}
	}
}