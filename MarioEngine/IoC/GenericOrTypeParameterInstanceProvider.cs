﻿using System;
using System.Linq;
using System.Reflection;
using Ninject.Extensions.Factory;
using Ninject.Parameters;

namespace MarioEngine.IoC
{
	public class GenericOrTypeParameterInstanceProvider : StandardInstanceProvider
	{
		protected override Type GetType( MethodInfo methodInfo, object[] arguments )
		{
			if ( methodInfo.ContainsGenericParameters )
			{
				return methodInfo.GetGenericArguments()[ 0 ];
			}

			var methodParameters = methodInfo.GetParameters();

			if ( methodParameters.Length >= 1 && typeof( Type ).IsAssignableFrom( methodParameters[ 0 ].ParameterType ) )
			{
				return (Type) arguments[ 0 ];
			}

			return base.GetType( methodInfo, arguments );
		}

		protected override IConstructorArgument[] GetConstructorArguments( MethodInfo methodInfo, object[] arguments )
		{
			var methodParameters = methodInfo.GetParameters();

			if ( methodParameters.Length >= 1 && typeof( Type ).IsAssignableFrom( methodParameters[ 0 ].ParameterType ) )
			{
				return base.GetConstructorArguments( methodInfo, arguments )[ 1.. ];
			}

			return base.GetConstructorArguments( methodInfo, arguments );
		}
	}
}