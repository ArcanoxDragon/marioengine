﻿using System;
using GameDotNet.Abstract.Engine;
using GameDotNet.Abstract.Services;
using GameDotNet.Extensions;
using MarioEngine.API;
using MarioEngine.API.Config;
using MarioEngine.API.Services;
using MarioEngine.API.Services.Tiles;
using MarioEngine.Config;
using MarioEngine.Engine.Plugins;
using MarioEngine.Menu;
using Ninject;
using OpenTK.Input;

namespace MarioEngine.Engine
{
	class MarioGame : GameWithStatesBase<IMarioGame, MarioState>, IMarioGame
	{
		public const string GameName = "Super Mario .NET";

		public MarioGame(
			ICommonService common,
			IGameRunner runner,
			IStateFactory stateFactory
		) : base( common, runner, stateFactory ) { }

		[ Inject ] public ITileRegistry TileRegistry { get; set; }
		[ Inject ] public IViewManager  ViewManager  { get; set; }

		[ Inject ] public PluginLoader PluginLoader { private get; set; }

		public string        Title             { get; set; } = "";
		public KeyboardState KeyboardState     { get; private set; }
		public MouseState    MouseState        { get; private set; }
		public KeyboardState LastKeyboardState { get; private set; }
		public MouseState    LastMouseState    { get; private set; }

		public IGameConfig Config => GameConfig.Instance;

		internal void RegisterIoCModules( IKernel kernel )
		{
			this.Logger.WriteLine( "Searching for plugins..." );
			this.PluginLoader.FindAndLoadPlugins();

			this.Logger.WriteLine( "Registering all plugin modules..." );

			foreach ( var plugin in this.PluginLoader.LoadedPlugins )
			foreach ( var contentProvider in plugin.ContentProviders )
				contentProvider.RegisterModules( kernel );
		}

		public override void Initialize()
		{
			base.Initialize();

			this.Logger.WriteLine( $"Loading content from {this.PluginLoader.LoadedPlugins.Count} plugins..." );
			this.PluginLoader.LoadAllContent();
		}

		public override void PostInitialize()
		{
			this.SwitchState<StateMenu>();
		}

		public bool GetKeyOneShot( Key key, bool onRelease = false )
			=> this.KeyboardState[ key ] ^ onRelease && !this.LastKeyboardState[ key ] ^ onRelease;

		public bool GetMouseOneShot( MouseButton button, bool onRelease = false )
			=> this.MouseState[ button ] ^ onRelease && !this.LastMouseState[ button ] ^ onRelease;

		protected override void PreUpdate( TimeSpan deltaTime )
		{
			this.LastKeyboardState = this.KeyboardState;
			this.LastMouseState    = this.MouseState;

			if ( this.Window.Focused )
			{
				this.KeyboardState = Keyboard.GetState();
				this.MouseState    = Mouse.GetState();
			}
			else
			{
				// Reset states to default; no input while window is not focused!

				this.KeyboardState = new KeyboardState();
				this.MouseState    = new MouseState();
			}
		}

		protected override void PostUpdate( TimeSpan deltaTime )
		{
			// Update title
			var displayFps   = (long) Math.Round( this.Runner.FramesPerSecond );
			var displayTps   = (long) Math.Round( this.Runner.TicksPerSecond );
			var displayTitle = this.Title.IsNullOrEmpty() ? "" : $" | {this.Title}";

			this.Runner.Title = $"{this.Runner.Options.GameName} ({displayFps} FPS | {displayTps} TPS){displayTitle}";

			// Update all texture animations
			this.Animations.UpdateAnimations( deltaTime );
		}
	}
}