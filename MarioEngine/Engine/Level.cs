﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Utility;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.API.Services;
using MarioEngine.API.Tiles;

namespace MarioEngine.Engine
{
	public class Level : ILevel
	{
		private const int ViewOverscanTiles = 2;

		private readonly IGameAccessor  gameAccessor;
		private readonly ITile[][]      renderCache;
		private readonly Queue<ITile>[] addQueue;
		private readonly Queue<ITile>[] removeQueue;
		private readonly Mutex          tilesMutex = new Mutex();

		private bool  needsToAdd;
		private bool  needsToRemove;
		private float cameraX;
		private float cameraY;

		public Level( IGameAccessor gameAccessor )
		{
			this.gameAccessor = gameAccessor;
			this.renderCache  = new ITile[ LayerConstants.Count ][];
			this.addQueue     = new Queue<ITile>[ LayerConstants.Count ];
			this.removeQueue  = new Queue<ITile>[ LayerConstants.Count ];

			this.CameraX    = 0.0f;
			this.CameraY    = 0.0f;
			this.ViewHeight = ViewConstants.DefaultViewHeightInTiles;
			this.Layers     = new List<ITile>[ LayerConstants.Count ];

			for ( var layer = 0; layer < this.Layers.Length; layer++ )
			{
				this.Layers[ layer ]      = new List<ITile>();
				this.renderCache[ layer ] = new ITile[ 0 ];
				this.addQueue[ layer ]    = new Queue<ITile>();
				this.removeQueue[ layer ] = new Queue<ITile>();
			}

			this.Random = new Random( (int) DateTimeUtil.MillisSinceEpoch() );
		}

		public IMarioGame        Game      => this.gameAccessor.Game;
		public ITransformManager Transform => this.Game.Transform;

		public Random        Random { get; }
		public List<ITile>[] Layers { get; }

		public float CameraX
		{
			get => this.cameraX;
			set
			{
				var halfZoomedWindowWidth = this.Game.Window.Width / ( 2.0f * this.ViewHeight );

				this.cameraX = Math.Max( Math.Min( value, halfZoomedWindowWidth ), 0.0f );
			}
		}

		public float CameraY
		{
			get => this.cameraY;
			set
			{
				var halfZoomedWindowHeight = this.Game.Window.Height / ( 2.0f * this.ViewHeight );

				this.cameraY = Math.Max( Math.Min( value, halfZoomedWindowHeight ), 0.0f );
			}
		}

		public float ViewWidth
		{
			get => this.ViewHeight * ( this.Game.Window.Width / (float) this.Game.Window.Height );
			set => this.ViewHeight = value * ( this.Game.Window.Height / (float) this.Game.Window.Width );
		}

		public float ViewHeight { get; set; }

		public ITile[] GetObjects( uint layer )
		{
			if ( layer >= LayerConstants.Count )
				return new ITile[] { };
			return this.Layers[ layer ].ToArray();
		}

		public void AddTile( uint layer, ITile tile )
		{
			if ( layer >= LayerConstants.Count )
				return;

			this.tilesMutex.WaitOne();

			if ( !this.addQueue[ layer ].Contains( tile ) )
			{
				this.addQueue[ layer ].Enqueue( tile );
				this.needsToAdd = true;
			}

			this.tilesMutex.ReleaseMutex();
		}

		public void RemoveTile( uint layer, ITile tile )
		{
			if ( layer >= LayerConstants.Count )
				return;

			this.tilesMutex.WaitOne();

			if ( !this.removeQueue[ layer ].Contains( tile ) )
			{
				this.removeQueue[ layer ].Enqueue( tile );
				this.needsToRemove = true;
			}

			this.tilesMutex.ReleaseMutex();
		}

		public void Update( TimeSpan deltaTime )
		{
			for ( uint layer = 0; layer < LayerConstants.Count; layer++ )
			{
				for ( int obj = 0; obj < this.Layers[ layer ].Count; obj++ )
					this.Layers[ layer ][ obj ].Update( deltaTime );

				if ( this.needsToAdd && this.addQueue[ layer ].Any() )
				{
					this.tilesMutex.WaitOne();

					while ( this.addQueue[ layer ].Any() )
					{
						var tile = this.addQueue[ layer ].Dequeue();

						if ( !this.Layers[ layer ].Contains( tile ) )
						{
							this.Layers[ layer ].Add( tile );
							tile.AddToLevel( this, layer );
						}
					}

					this.tilesMutex.ReleaseMutex();
					this.addQueue[ layer ].Clear();
				}

				if ( this.needsToRemove && this.removeQueue[ layer ].Any() )
				{
					this.tilesMutex.WaitOne();

					while ( this.removeQueue[ layer ].Any() )
					{
						var tile = this.removeQueue[ layer ].Dequeue();

						if ( this.Layers[ layer ].Contains( tile ) )
						{
							tile.RemoveFromLevel();
							this.Layers[ layer ].Remove( tile );
						}
					}

					this.tilesMutex.ReleaseMutex();
					this.removeQueue[ layer ].Clear();
				}
			}

			this.needsToAdd    = false;
			this.needsToRemove = false;
		}

		public void Render( TimeSpan deltaTime )
		{
			var minX = this.CameraX - ViewOverscanTiles;
			var minY = this.CameraY - ViewOverscanTiles;
			var maxX = this.CameraX + this.ViewWidth + ViewOverscanTiles;
			var maxY = this.CameraY + this.ViewHeight + ViewOverscanTiles;

			this.tilesMutex.WaitOne();

			this.Transform.PushMatrix();
			this.Transform.LoadIdentity();
			this.Game.ViewManager.SetProjectionForLevel( this.CameraX, this.CameraY, this.ViewHeight );
			{
				for ( uint layer = 0; layer < LayerConstants.Count; layer++ )
				{
					foreach ( var tile in this.Layers[ layer ] )
					{
						if ( tile.X + tile.Width >= minX && tile.X <= maxX &&
							 tile.Y + tile.Height >= minY && tile.Y <= maxY )
						{
							this.Transform.PushMatrix();
							this.Transform.Translate( tile.X, tile.Y, 0.0f );

							tile.Render( deltaTime );

							this.Transform.PopMatrix();
						}
					}
				}
			}
			this.Transform.PopMatrix();

			this.tilesMutex.ReleaseMutex();
		}
	}
}