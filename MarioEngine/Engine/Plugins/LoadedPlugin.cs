﻿using System.Collections.Generic;
using System.Runtime.Loader;
using MarioEngine.API;

namespace MarioEngine.Engine.Plugins
{
	public class LoadedPlugin
	{
		public LoadedPlugin( AssemblyLoadContext context, IList<IContentProvider> contentProviders )
		{
			this.Context          = context;
			this.ContentProviders = (IReadOnlyList<IContentProvider>) contentProviders;
		}

		public AssemblyLoadContext             Context          { get; }
		public IReadOnlyList<IContentProvider> ContentProviders { get; }
	}
}