﻿using System.Reflection;
using System.Runtime.Loader;

namespace MarioEngine.Engine.Plugins
{
	public class PluginLoadContext : AssemblyLoadContext
	{
		protected override Assembly Load( AssemblyName assemblyName )
			=> Default.LoadFromAssemblyName( assemblyName );
	}
}