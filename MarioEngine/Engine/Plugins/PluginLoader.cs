﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using GameDotNet.Abstract.Services;
using MarioEngine.Abstract.Services;
using MarioEngine.API;
using MarioEngine.API.Attributes;

namespace MarioEngine.Engine.Plugins
{
	class PluginLoader
	{
		private readonly List<LoadedPlugin>      loadedPlugins;
		private readonly IContentProviderFactory contentProviderFactory;
		private readonly ILogger                 logger;

		public PluginLoader( IContentProviderFactory contentProviderFactory,
							 ILogger logger )
		{
			this.loadedPlugins          = new List<LoadedPlugin>();
			this.contentProviderFactory = contentProviderFactory;
			this.logger                 = logger;
		}

		public IReadOnlyList<LoadedPlugin> LoadedPlugins => this.loadedPlugins;

		public void UnloadAllPlugins()
		{
			this.logger.WriteLine( $"Unloading {this.loadedPlugins.Count} plugins..." );

			foreach ( var plugin in this.loadedPlugins )
			{
				plugin.Context.Unload();
			}

			this.loadedPlugins.Clear();
			GC.Collect( GC.MaxGeneration, GCCollectionMode.Forced );
			this.logger.WriteLine( "All plugins were unloaded" );
		}

		public void FindAndLoadPlugins()
		{
			if ( this.loadedPlugins.Any() )
				this.UnloadAllPlugins();

			if ( Directory.Exists( "Plugins" ) )
			{
				foreach ( string pluginFile in Directory.EnumerateFiles( "Plugins", "*.dll" ) )
				{
					var fullPluginPath = Path.GetFullPath( pluginFile );
					var pluginContext  = new PluginLoadContext();
					var pluginAssembly = pluginContext.LoadFromAssemblyPath( fullPluginPath );

					if ( pluginAssembly == default )
					{
						this.logger.WriteLine( $"Could not load assembly from plugin \"{pluginFile}\"" );
						continue;
					}

					var contentProviders = new List<IContentProvider>();

					foreach ( var contentProviderType in from type in pluginAssembly.DefinedTypes
														 where type.IsClass
														 where typeof( IContentProvider ).IsAssignableFrom( type )
														 where type.GetCustomAttribute<PluginAttribute>() != null
														 select type )
					{
						var contentProviderInst = this.contentProviderFactory.Create( contentProviderType );

						if ( !( contentProviderInst is IContentProvider contentProvider ) )
						{
							this.logger.WriteLine( $"Instantiated plugin type \"{contentProviderType.FullName}\" " +
												   $"was unable to be cast to {typeof( IContentProvider ).FullName}" );
							continue;
						}

						this.logger.WriteLine( $"Successfully instantiated content provider plugin \"{contentProvider.Name}\"" );
						contentProviders.Add( contentProvider );
					}

					this.logger.WriteLine( $"Found {contentProviders.Count} content provider plugins in {pluginFile}" );

					this.loadedPlugins.Add( new LoadedPlugin( pluginContext, contentProviders ) );
				}
			}
		}

		public void LoadAllContent()
		{
			foreach ( var plugin in this.LoadedPlugins )
			foreach ( var contentProvider in plugin.ContentProviders )
			{
				contentProvider.LoadContent();
			}
		}
	}
}