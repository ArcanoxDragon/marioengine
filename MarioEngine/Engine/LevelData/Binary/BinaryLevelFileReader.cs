﻿using System.Collections.Generic;
using System.IO;
using GameDotNet.Extensions;
using MarioEngine.Abstract.Services;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.API.Services;
using MarioEngine.API.Tiles;
using MarioEngine.Exceptions;

namespace MarioEngine.Engine.LevelData.Binary
{
	class BinaryLevelFileReader : ILevelFileReader
	{
		private readonly ILevelFactory levelFactory;
		private readonly IGameAccessor gameAccessor;

		public BinaryLevelFileReader( ILevelFactory levelFactory, IGameAccessor gameAccessor )
		{
			this.levelFactory = levelFactory;
			this.gameAccessor = gameAccessor;
		}

		protected IMarioGame Game => this.gameAccessor.Game;

		public Level LoadLevel( string filename )
		{
			var filePath = filename;

			if ( !this.Game.Resources.ResourceExists( filePath ) )
			{
				this.Game.Logger.WriteLine( $"Level file not found: {filePath}" );
				return null;
			}

			this.Game.Logger.WriteLine( $"Loading level from \"{filePath}\"..." );

			using ( var levelStream = this.Game.Resources.GetResourceStream( filePath ) )
			using ( var reader = new BinaryReader( levelStream ) )
			{
				var version = this.ReadVersion( reader );

				if ( version > LevelConstants.CurrentBinaryFormatVersion )
					throw new LevelLoadException( $"Level file has a newer version ({version}) than the game ({LevelConstants.CurrentBinaryFormatVersion}) and cannot be loaded." );

				var level = this.levelFactory.Create( this.Game );

				foreach ( var (layer, tile) in this.ReadTiles( reader ) )
					level.AddTile( layer, tile );

				return level;
			}
		}

		private int ReadVersion( BinaryReader reader )
		{
			int version;

			if ( reader.BaseStream.GetAvailable() < 2 )
				throw new LevelLoadException( "Empty or unreadable level file" );

			version = reader.ReadInt16();

			this.Game.Logger.WriteLine( $"Level file version: {version}" );

			return version;
		}

		private IEnumerable<(uint, ITile)> ReadTiles( BinaryReader reader )
		{
			var tileTypeMap = this.ReadDefinedTypes( reader );

			while ( reader.BaseStream.GetAvailable() >= 12 ) // at least a 4-byte ID, 4-byte layer, and 4-byte length
			{
				var typeId = reader.ReadUInt32();
				var layer  = reader.ReadUInt32();
				var length = reader.ReadUInt32();

				if ( tileTypeMap.TryGetValue( typeId, out var tileType ) )
				{
					if ( reader.BaseStream.GetAvailable() < length )
						throw new LevelLoadException( $"Encountered unexpected end of file while loading tile of type \"{tileType}\"" );

					var tile = this.Game.TileRegistry.ReadTile( reader, tileType );

					if ( tile != null )
						yield return ( layer, tile );
					else
						this.Game.Logger.WriteLine( $"Tile loader returned null tile for type \"{tileType}\"" );
				}
				else
				{
					this.Game.Logger.WriteLine( $"Found tile with type ID \"{typeId}\" but no such type was found in the type table. Ignoring tile." );

					reader.BaseStream.Seek( length, SeekOrigin.Current );
				}
			}
		}

		private Dictionary<uint, string> ReadDefinedTypes( BinaryReader reader )
		{
			var idToNameMap = new Dictionary<uint, string>();
			var nameToIdMap = new Dictionary<string, uint>();

			if ( reader.BaseStream.GetAvailable() < 2 )
				throw new LevelLoadException( "No tile types or tile objects were found in level" );

			var numTypes = reader.ReadInt16();

			if ( numTypes <= 0 )
			{
				this.Game.Logger.WriteLine( "Level has no defined tile types and will be empty!" );
				return idToNameMap;
			}

			for ( var i = 0; i < numTypes; i++ )
			{
				if ( reader.BaseStream.GetAvailable() < 5 )
					throw new LevelLoadException( $"Encountered unexpected end of file while loading type table (expected {numTypes} types; found {idToNameMap.Count})" );

				var typeId   = reader.ReadUInt32();
				var typeName = reader.ReadString();

				if ( idToNameMap.ContainsKey( typeId ) )
					throw new LevelLoadException( $"Encountered duplicate type ID: \"{typeId}\"" );
				if ( nameToIdMap.ContainsKey( typeName ) )
					throw new LevelLoadException( $"Encountered duplicate type name: \"{typeName}\"" );

				idToNameMap[ typeId ]   = typeName;
				nameToIdMap[ typeName ] = typeId;
			}

			return idToNameMap;
		}
	}
}