﻿using System;
using System.IO;
using MarioEngine.API;
using MarioEngine.API.Services;

namespace MarioEngine.Engine.LevelData.Binary
{
	class BinaryLevelFileWriter : ILevelFileWriter
	{
		private readonly IGameAccessor gameAccessor;

		public BinaryLevelFileWriter( IGameAccessor gameAccessor )
		{
			this.gameAccessor = gameAccessor;
		}

		protected IMarioGame Game => this.gameAccessor.Game;

		public void SaveLevel( Level level, string filename )
		{
			try
			{
				string path = Path.GetFullPath( filename );

				if ( File.Exists( path ) )
					File.Delete( path );
			}
			catch ( Exception ex )
			{
				this.Game.Logger.WriteLine( $"Error while saving level:\n\n{ex}\n" );
			}
		}
	}
}