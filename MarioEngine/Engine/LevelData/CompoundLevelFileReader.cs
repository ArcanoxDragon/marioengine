﻿using System;
using System.IO;
using Ninject;

namespace MarioEngine.Engine.LevelData
{
	class CompoundLevelFileReader : ILevelFileReader
	{
		private readonly ILevelFileReader binaryReader;
		private readonly ILevelFileReader jsonReader;

		public CompoundLevelFileReader( [ Named( "Binary" ) ] ILevelFileReader binaryReader,
										[ Named( "Json" ) ] ILevelFileReader jsonReader )
		{
			this.binaryReader = binaryReader;
			this.jsonReader   = jsonReader;
		}

		public Level LoadLevel( string filename )
		{
			var extension = Path.GetExtension( filename )?.ToLower();
			var loader = extension switch {
				 ".wld" => this.binaryReader,
				 ".json" => this.jsonReader,
				 _ => throw new NotSupportedException( $"The file extension \"{Path.GetExtension( filename )}\" is not supported" )
			 };

			return loader.LoadLevel( filename );
		}
	}
}