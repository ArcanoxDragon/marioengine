﻿namespace MarioEngine.Engine.LevelData
{
	interface ILevelFileWriter
	{
		void SaveLevel( Level level, string filename );
	}
}