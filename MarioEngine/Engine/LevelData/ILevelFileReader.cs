﻿namespace MarioEngine.Engine.LevelData
{
	interface ILevelFileReader
	{
		Level LoadLevel( string filename );
	}
}