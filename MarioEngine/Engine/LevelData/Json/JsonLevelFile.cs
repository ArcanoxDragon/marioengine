﻿using System.Collections.Generic;

namespace MarioEngine.Engine.LevelData.Json
{
	public class JsonLevelFile
	{
		public int                      Version   { get; set; }
		public Dictionary<uint, string> TileTypes { get; set; }
		public List<JsonLevelLayer>     Layers    { get; set; }
	}

	public class JsonLevelLayer
	{
		public uint                Id    { get; set; }
		public List<JsonLevelTile> Tiles { get; set; }
	}

	public class JsonLevelTile
	{
		public uint   Type { get; set; }
		public string Data { get; set; }
	}
}