﻿using System;
using System.IO;
using MarioEngine.Abstract.Services;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.API.Services;
using MarioEngine.Engine.LevelData.Binary;
using MarioEngine.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MarioEngine.Engine.LevelData.Json
{
	class JsonLevelFileReader : ILevelFileReader
	{
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
		};

		private readonly ILevelFactory levelFactory;
		private readonly IGameAccessor gameAccessor;

		public JsonLevelFileReader( ILevelFactory levelFactory, IGameAccessor gameAccessor )
		{
			this.levelFactory = levelFactory;
			this.gameAccessor = gameAccessor;
		}

		protected IMarioGame Game => this.gameAccessor.Game;

		public Level LoadLevel( string filename )
		{
			var filePath = filename;

			if ( !this.Game.Resources.ResourceExists( filePath ) )
			{
				this.Game.Logger.WriteLine( $"Level file not found: {filePath}" );
				return null;
			}

			this.Game.Logger.WriteLine( $"Loading level from \"{filePath}\"..." );

			using ( var levelStream = this.Game.Resources.GetResourceStream( filePath ) )
			using ( var reader = new StreamReader( levelStream ) )
			{
				var levelJson = reader.ReadToEnd();
				var levelObj  = JsonConvert.DeserializeObject<JsonLevelFile>( levelJson, JsonSettings );

				if ( levelObj.Version > LevelConstants.CurrentJsonFormatVersion )
					throw new LevelLoadException( $"Level file has a newer version ({levelObj.Version}) " +
												  $"than the game ({LevelConstants.CurrentJsonFormatVersion}) and cannot be loaded." );

				var level = this.levelFactory.Create( this.Game );

				foreach ( var layer in levelObj.Layers )
				foreach ( var tileObj in layer.Tiles )
				{
					if ( levelObj.TileTypes.TryGetValue( tileObj.Type, out var tileType ) )
					{
						var tileData = Convert.FromBase64String( tileObj.Data );

						using ( var tileStream = new MemoryStream( tileData ) )
						using ( var tileReader = new BinaryReader( tileStream ) )
						{
							var tile = this.Game.TileRegistry.ReadTile( tileReader, tileType );

							if ( tile == null )
								this.Game.Logger.WriteLine( $"Tile loader returned null tile for type \"{tileType}\"" );
							else
								level.AddTile( layer.Id, tile );
						}
					}
					else
					{
						this.Game.Logger.WriteLine( $"Found tile with type ID \"{tileObj.Type}\" but no such type was found in the type table. Ignoring tile." );
					}
				}

				return level;
			}
		}
	}
}