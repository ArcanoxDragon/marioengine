﻿using System;
using System.Collections.Generic;
using GameDotNet.Audio;
using GameDotNet.Graphics;
using MarioEngine.API;
using MarioEngine.Gui.PauseMenu;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace MarioEngine.Engine.States
{
	public abstract class StateWithPauseMenu : MarioState
	{
		private bool pauseKeyLock;

		protected StateWithPauseMenu( IMarioGame game ) : base( game )
		{
			this.PauseMenu = new PauseMenu( this );
		}

		public bool IsPaused { get; protected set; }

		protected PauseMenu PauseMenu  { get; }
		protected Shader    GuiShader  { get; set; }
		protected Sound     PauseSound { get; set; }

		public abstract string PauseTitle { get; }

		public abstract IEnumerable<PauseMenuAction> GetPauseMenuActions();

		protected override void UnloadAllResources()
		{
			base.UnloadAllResources();

			this.GuiShader = null;
			this.PauseSound = null;
		}

		public void PauseGame()
		{
			this.IsPaused = true;
			this.PauseSound.Play( this.EffectSource );
			this.MusicSource.Volume = this.Game.Config.IsMusicEnabled ? 0.25f : 0.0f;
		}

		public void UnpauseGame()
		{
			this.IsPaused = false;
			this.PauseSound.Play( this.EffectSource );
			this.MusicSource.Volume = this.Game.Config.IsMusicEnabled ? 1.0f : 0.0f;
		}

		public void TogglePaused()
		{
			if ( this.IsPaused )
				this.UnpauseGame();
			else
				this.PauseGame();
		}

		protected override void InitializeTickResourceIfNeeded()
		{
			base.InitializeTickResourceIfNeeded();

			if ( this.PauseSound == null )
				this.PauseSound = this.Game.Sounds.Get( "Pause.wav" );
		}

		public sealed override void Update( TimeSpan deltaTime )
		{
			base.Update( deltaTime );

			// Pauses on key press, unpauses on key release
			if ( this.Game.GetKeyOneShot( Key.Escape, this.IsPaused ) && !this.pauseKeyLock )
			{
				// Since the one-shot behavior changes depending on whether or not the game is paused,
				// we lock the pause/unpause trigger until the key is released again
				this.pauseKeyLock = true;
				this.TogglePaused();
			}

			if ( !this.Game.KeyboardState[ Key.Escape ] )
			{
				this.pauseKeyLock = false;
			}

			if ( this.IsPaused )
			{
				this.UpdateWhilePaused( deltaTime );
				this.PauseMenu.Update( deltaTime );
			}
			else
			{
				this.UpdateWhileNotPaused( deltaTime );
			}

			this.UpdateAlways( deltaTime );
		}

		protected virtual void UpdateWhilePaused( TimeSpan deltaTime ) { }
		protected virtual void UpdateWhileNotPaused( TimeSpan deltaTime ) { }
		protected virtual void UpdateAlways( TimeSpan deltaTime ) { }

		protected override void InitializeRenderResourcesIfNeeded()
		{
			base.InitializeRenderResourcesIfNeeded();

			if ( this.GuiShader == null )
				this.GuiShader = this.Game.Shaders.Get( "gui" );
		}

		public sealed override void Render( TimeSpan deltaTime )
		{
			base.Render( deltaTime );

			if ( this.IsPaused )
			{
				this.RenderWhilePaused( deltaTime );
			}
			else
			{
				this.RenderWhileNotPaused( deltaTime );
			}

			this.RenderAlways( deltaTime );

			if ( this.IsPaused )
			{
				this.GuiShader.Bind();
				this.Game.ViewManager.SetProjectionForGui();
				GL.Disable( EnableCap.DepthTest );
				this.PauseMenu.Render( deltaTime );
			}
		}

		protected virtual void RenderWhilePaused( TimeSpan deltaTime ) { }
		protected virtual void RenderWhileNotPaused( TimeSpan deltaTime ) { }
		protected virtual void RenderAlways( TimeSpan deltaTime ) { }
	}
}