﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameDotNet.Abstract.Services;
using MarioEngine.API.Services.Tiles;
using MarioEngine.API.Tiles;

namespace MarioEngine.Engine
{
	public class TileRegistry : ITileRegistry
	{
		private readonly ITileHandlerFactory tileHandlerFactory;
		private readonly List<ITileHandler>  handlers;

		public TileRegistry( ITileHandlerFactory tileHandlerFactory, ILogger logger )
		{
			this.tileHandlerFactory = tileHandlerFactory;
			this.handlers           = new List<ITileHandler>();

			this.Logger = logger;
		}

		public ILogger Logger { get; }

		public void ClearRegistry()
		{
			this.handlers.Clear();
		}

		public void RegisterTileHandler<THandler>( THandler handler )
			where THandler : ITileHandler
		{
			this.handlers.Add( handler );
		}

		public void RegisterTileHandler<THandler>()
			where THandler : ITileHandler
		{
			var handlerInstance = this.tileHandlerFactory.Create<THandler>();

			this.RegisterTileHandler( handlerInstance );
		}

		public bool CanHandleType( string typeName ) => this.handlers.Any( h => h.HandlesType( typeName ) );

		public ITileHandler GetHandlerFor( string typeName )
			=> this.handlers.FirstOrDefault( h => h.HandlesType( typeName ) );

		public ITile ReadTile( BinaryReader reader, string typeName )
		{
			if ( !this.CanHandleType( typeName ) )
				throw new InvalidOperationException( $"There is no tile handler registered for tile type \"{typeName}\"" );

			var handler    = this.GetHandlerFor( typeName );
			var returnTile = handler.ReadTile( reader, typeName );

			if ( returnTile == null )
				return null;

			if ( returnTile.Type != typeName )
			{
				this.Logger.WriteLine( $"Handler \"{handler.GetType().Name}\" returned a tile whose type " +
									   $"(\"{returnTile.Type}\") did not match the requested type (\"{typeName}\")." );
				return null;
			}

			return returnTile;
		}

		public void WriteTile( BinaryWriter writer, ITile tile )
		{
			if ( !this.CanHandleType( tile.Type ) )
				throw new InvalidOperationException( $"There is no tile handler registered for tile type \"{tile.Type}\"" );

			var handler = this.GetHandlerFor( tile.Type );

			handler.WriteTile( writer, tile );
		}
	}
}