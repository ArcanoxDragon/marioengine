﻿using System;
using System.Collections.Generic;
using GameDotNet.Graphics;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.Engine;
using MarioEngine.Engine.LevelData;
using MarioEngine.Gameplay.States;
using MarioEngine.Gui.PauseMenu;
using MarioEngine.Menu;
using Ninject;

namespace MarioEngine.Editor.States
{
	class StateEditor : StateLevel
	{
		private Level editingLevel;

		public StateEditor(
			IMarioGame game,
			[ Named( "Compound" ) ] ILevelFileReader levelReader
		) : base( game, levelReader ) { }

		protected Texture SelectionRectangle { get; private set; }

		protected override void UnloadAllResources()
		{
			base.UnloadAllResources();

			this.SelectionRectangle = null;
		}

		protected override void InitializeRenderResourcesIfNeeded()
		{
			base.InitializeRenderResourcesIfNeeded();
			if ( this.SelectionRectangle == null )
				this.SelectionRectangle = this.Game.Textures.GetLibrary().GetTexture( "Editor/Selection" );
		}

		protected override void OnEntered()
		{
			base.OnEntered();

			this.editingLevel = this.Level;
			this.Level        = null; // Grab the level so we control it and not the parent state
		}

		protected override void UpdateAlways( TimeSpan deltaTime )
		{
			this.editingLevel?.Update( deltaTime );
		}

		protected override void RenderAlways( TimeSpan deltaTime )
		{
			base.RenderAlways( deltaTime );

			this.Transform.PushMatrix();
			{
				this.Transform.Translate( 0, 0, 0.2f );
				this.GuiShader.Bind();
				this.SelectionRectangle.Bind();

				if ( this.editingLevel != null )
				{
					foreach ( var tile in this.editingLevel.GetObjects( 0 ) )
					{
						this.Game.Gl.RenderBorderedPlane( tile.X * TileConstants.StandardSize,
														  tile.Y * TileConstants.StandardSize,
														  tile.Width * TileConstants.StandardSize,
														  tile.Height * TileConstants.StandardSize,
														  this.SelectionRectangle.Width / 3 );
					}
				}
			}
			this.Transform.PopMatrix();
		}

		#region Pause Menu

		public override string PauseTitle => "Editor Paused";

		private void OnSaveAndContinue()
		{
			// TODO: Save level

			this.UnpauseGame();
		}

		private void OnSaveAndExit()
		{
			// TODO: Save level

			this.Game.SwitchState<StateMenu>();
		}

		public override IEnumerable<PauseMenuAction> GetPauseMenuActions()
		{
			yield return new PauseMenuAction( "Continue",          this.UnpauseGame );
			yield return new PauseMenuAction( "Save and Continue", this.OnSaveAndContinue );
			yield return new PauseMenuAction( "Save and Exit",     this.OnSaveAndExit );
		}

		#endregion
	}
}