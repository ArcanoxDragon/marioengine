using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using GameDotNet.Abstract.Services.Audio;
using GameDotNet.Graphics;
using GameDotNet.Utility.Graphics;
using MarioEngine.API;
using MarioEngine.API.Constants;
using MarioEngine.Config;
using MarioEngine.Editor.States;
using MarioEngine.Gameplay.States;
using MarioEngine.Gui;
using Font = GameDotNet.Graphics.Font;

namespace MarioEngine.Menu
{
	public class StateMenu : MarioState
	{
		private const string LabelSoundOn  = "\x80";
		private const string LabelMusicOn  = "\x81";
		private const string LabelSoundOff = "\x82";
		private const string LabelMusicOff = "\x83";

		private readonly IAudioContextProvider audioContextProvider;
		private readonly GuiScreen             gui;

		private string titleText;
		private bool   needsResize = true;
		private int    centerX;

		#region Controls

		private Button buttonStart;
		private Button buttonEditor;
		private Button buttonQuit;

		private Button buttonMusic;
		private Button buttonSound;

		private ControlRow buttonRowSound;

		#endregion Controls

		public StateMenu( IMarioGame game, IAudioContextProvider audioContextProvider ) : base( game )
		{
			this.audioContextProvider = audioContextProvider;

			this.gui = new GuiScreen( this );
		}

		public override string Name           => StateNames.Menu;
		public override string SoundtrackName => "Title";

		protected Font   LogoFont   { get; private set; }
		protected Shader MenuShader { get; private set; }

		protected override void OnEntered()
		{
			this.titleText = this.Game.Runner.Options.GameName;

			this.Game.Window.Resize += this.GameWindowResized;

			#region Buttons

			this.buttonStart = new Button( this.gui ) {
				Size = GuiConstants.Buttons.ButtonSize,
				Text = "Start Game"
			};
			this.buttonEditor = new Button( this.gui ) {
				Size = GuiConstants.Buttons.ButtonSize,
				Text = "Level Editor"
			};
			this.buttonQuit = new Button( this.gui ) {
				Size = GuiConstants.Buttons.ButtonSize,
				Text = "Quit Game"
			};
			this.buttonMusic = new Button( this.gui ) {
				Size               = new Size( 48, 48 ),
				ShowSelectionArrow = false,
				Text               = GameConfig.Instance.IsMusicEnabled ? LabelMusicOn : LabelMusicOff
			};
			this.buttonSound = new Button( this.gui ) {
				Size               = new Size( 48, 48 ),
				ShowSelectionArrow = false,
				Text               = GameConfig.Instance.IsSoundEnabled ? LabelSoundOn : LabelSoundOff
			};

			this.buttonStart.OnPressed  += this.OnButtonStartPressed;
			this.buttonEditor.OnPressed += this.OnButtonEditorPressed;
			this.buttonQuit.OnPressed   += this.OnButtonQuitPressed;
			this.buttonMusic.OnPressed  += this.OnButtonMusicPressed;
			this.buttonSound.OnPressed  += this.buttonSound_Pressed;

			this.buttonRowSound = new ControlRow( this.gui );
			this.buttonRowSound.AddControl( this.buttonMusic );
			this.buttonRowSound.AddControl( this.buttonSound );

			this.gui.AddControl( this.buttonStart );
			this.gui.AddControl( this.buttonEditor );
			this.gui.AddControl( this.buttonQuit );
			this.gui.AddControl( this.buttonRowSound );

			#endregion Buttons
		}

		protected override void OnLeft() { }

		private void OnButtonStartPressed( Button sender )
		{
			this.Game.SwitchState<StateLevel>();
		}

		private void OnButtonEditorPressed( Button sender )
		{
			this.Game.SwitchState<StateEditor>();
		}

		private void OnButtonQuitPressed( Button sender )
		{
			this.Game.Runner.Exit();
		}

		private void OnButtonMusicPressed( Button sender )
		{
			this.Game.Config.IsMusicEnabled = !this.Game.Config.IsMusicEnabled;

			if ( this.MusicSource != null )
				this.MusicSource.Volume = this.Game.Config.IsMusicEnabled ? 1.0f : 0.0f;
		}

		private void buttonSound_Pressed( Button sender )
		{
			this.Game.Config.IsSoundEnabled = !this.Game.Config.IsSoundEnabled;
		}

		private void GameWindowResized( object sender, EventArgs e )
		{
			this.needsResize = true;
		}

		public override void Update( TimeSpan deltaTime )
		{
			base.Update( deltaTime );

			this.audioContextProvider.Context?.MakeCurrent();

			this.buttonMusic.Text = this.Game.Config.IsMusicEnabled ? LabelMusicOn : LabelMusicOff;
			this.buttonSound.Text = this.Game.Config.IsSoundEnabled ? LabelSoundOn : LabelSoundOff;

			if ( this.Game.Window.Focused )
			{
				if ( this.Game.GetKeyOneShot( Key.Escape, true ) )
				{
					this.Game.Runner.Exit();
				}

				this.gui.UpdateScreen();
			}
		}

		public override void Render( TimeSpan deltaTime )
		{
			if ( this.needsResize )
			{
				// Set projection matrix

				this.needsResize = false;

				var proj = Matrix4.Identity
						   * Matrix4.CreateScale( 1.0f / this.Game.Window.Width,
												  1.0f / this.Game.Window.Height,
												  1.0f )
						   * Matrix4.CreateScale( 2.0f, -2.0f, 1.0f )
						   * Matrix4.CreateTranslation( -1.0f, 1.0f, 0.0f );

				this.Game.Transform.SetProjection( proj );
				GL.Viewport( 0, 0, this.Game.Window.Width, this.Game.Window.Height );
				this.centerX = this.Game.Window.Width / 2;

				#region Position buttons

				var controls = this.gui.Controls;

				for ( int control = 0; control < controls.Count; control++ )
				{
					controls[ control ].X = this.centerX - controls[ control ].Width / 2;
					controls[ control ].Y = 256 + control * ( controls[ control ].Height + 8 );
				}

				this.buttonRowSound.X = this.Game.Window.Width - ( this.buttonRowSound.Width + 8 );
				this.buttonRowSound.Y = this.Game.Window.Height - ( this.buttonRowSound.Height + 8 );

				#endregion
			}

			if ( this.LogoFont == null )
				this.LogoFont = this.Game.Fonts.Get( "Title" );
			if ( this.MenuShader == null )
				this.MenuShader = this.Game.Shaders.Get( "gui" );

			this.MenuShader.Bind();

			GL.ClearColor( Color4.SkyBlue );
			GL.Clear( ClearBufferMask.ColorBufferBit );

			var       textX = this.centerX - this.LogoFont.GetStringWidth( this.titleText ) / 2;
			const int textY = 64;

			this.Game.Shaders.Current.SetUniform( "tintColor", Color4.White );
			this.Game.Transform.PushMatrix();
			{
				this.Game.Transform.Scale( this.Game.Window.Width, this.Game.Window.Height, 1.0f );
				this.Game.Textures.GetLibrary().GetTexture( "Menu/Background" ).Bind();
				this.Game.Shapes.Plane.Render();
			}
			this.Game.Transform.PopMatrix();

			this.LogoFont.RenderString( this.titleText, textX, textY );
			this.gui.RenderScreen();
		}
	}
}