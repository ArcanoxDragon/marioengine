#version 150

in vec4 vertexPos;
in vec2 vertexUV;

uniform mat4 transform, modelView, projection, modelViewProjection;
uniform mat4 normalTransform, uvTransform;

out vec4 vPos, oVPos;
out vec2 vUV;

void main()
{
	vPos = modelView * vertexPos;

	vUV = (uvTransform * vec4(vertexUV, 0.0, 1.0)).st;
	
	gl_Position = modelViewProjection * vertexPos;
}