#version 150

in vec4 vertexPos;
in vec3 vertexTangent;
in vec3 vertexBinormal;
in vec3 vertexNormal;
in vec2 vertexUV;

uniform mat4 transform, modelView, modelViewProjection, depthVP;
uniform mat4 normalTransform, uvTransform;

out vec4 vPos, sPos;
out vec3 tangent, binormal, normal;
out mat3 tbn;
out vec2 vUV;

void main()
{
	vPos = modelView * vertexPos;
	normal = normalize(mat3(normalTransform) * vertexNormal);
	tangent = normalize(mat3(normalTransform) * vertexTangent);
	binormal = vertexBinormal;
	
	tbn = mat3(tangent, binormal, normal);

	vUV = (uvTransform * vec4(vertexUV.s, -vertexUV.t, 0.0, 1.0)).st;
	
	gl_Position = modelViewProjection * vertexPos;
}
