#version 150
#define NEAR 1.0
#define FAR  100.0
#define SHADOW_SAMPLES 32.0
#define NORMAL_MAP_AMOUNT 0.05
#define AMBIENT_BRIGHTNESS 0.85
#define SUN_BRIGHTNESS 0.2
#define MAX_LIGHTS 128 // Hard-coded in GameDotNet.Services.Graphics.LightManager.cs!

in vec4 vPos, sPos;
in vec3 tangent, binormal, normal, camPos;
in mat3 tbn;
in vec2 vUV;

uniform int useBump, useSpec;
uniform int numLights, bufIndex;
uniform sampler2D tex, bump, spec;
uniform sampler2D dirShadow;
uniform samplerCube[4] pointShadow;
uniform vec3 ambientGlobal = vec3(AMBIENT_BRIGHTNESS, AMBIENT_BRIGHTNESS, AMBIENT_BRIGHTNESS);
uniform vec3 sunDiffuse = vec3(1.0 * SUN_BRIGHTNESS, 0.98 * SUN_BRIGHTNESS, 0.98 * SUN_BRIGHTNESS);
uniform vec3 sunPos = vec3(1.0, -1.0, -5);

struct Light
{
    vec4 position;
    vec3 diffuse;
	vec3 ambient;
	float attenConst;
	float attenLinear;
	float attenQuad;
	float shadow;
};

uniform lights
{
	Light light[MAX_LIGHTS];
};

out vec4 fragmentColor;

float linearizeDepth(float depth)
{
	return (NEAR * 2.0) / (FAR + NEAR - depth * (FAR - NEAR));
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

void main()
{
	vec3 n, lightR, lightDir;
    float NdotL, NdotHV;
    vec3 lightDiff = ambientGlobal;
	vec3 lightSpec = vec3(0.0);
	vec4 texCol = texture(tex, vUV);
	vec4 specCol = texture(spec, vUV);
    float att, dist, specVal;

	n = normalize(normal);

	if (gl_FrontFacing) // Y+ is down, so faces are backwards naturally >_>
		n = -n;

	vec3 bumpTex = texture(bump, vUV).rgb * 2.0 - vec3(1.0);
	bumpTex = tbn * (bumpTex * NORMAL_MAP_AMOUNT);

	n = normalize(mix(n, n + bumpTex, useBump));

	// Sun
	lightDir = -normalize(sunPos);
	lightR = reflect(-lightDir, n);
	NdotL = clamp(dot(n, lightDir), 0.001, 0.999);
	specVal = pow(max(0.0, dot(lightR, normalize(-vPos.xyz))), 2.0);
	specVal *= mix(0.0, specCol.r, useSpec);
	lightSpec += sunDiffuse * 2.0 * specVal;
	lightDiff += (sunDiffuse * NdotL);

	for (int i = 0; i < numLights; i++)
	{
		if (abs(light[i].position.w - 1.0) < 1e-5) // Point light (w = 1.0)
		{
			lightDir = vec3(light[i].position - vPos);
			dist = length(lightDir);
			lightDir = normalize(lightDir);
			lightR = normalize(reflect(-lightDir, n));
			NdotL = clamp(dot(n, lightDir), 0.01, 0.99);

			float c = light[i].attenConst;
			float l = light[i].attenLinear;
			float q = light[i].attenQuad;

			att = clamp(1.0 / (c + l * dist + q * dist * dist), 0.0, 1.0);
			lightDiff += att * (light[i].diffuse * NdotL + light[i].ambient);

			specVal = att * pow(max(0.0, dot(lightR, normalize(-vPos.xyz))), 2.0);
			specVal *= mix(0.0, specCol.r, useSpec);
			lightSpec += light[i].diffuse * max(specVal, 0.0);
		}
		else if (abs(light[i].position.w) < 1e-5) // Directional light (w = 0.0)
		{
			lightDir = -normalize(light[i].position.xyz);
			lightR = reflect(-lightDir, n);
			NdotL = clamp(dot(n, lightDir), 0.01, 0.99);
			specVal = pow(max(0.0, dot(lightR, normalize(-vPos.xyz))), 2.0);
			specVal *= mix(0.0, specCol.r, useSpec);
			lightSpec += light[i].diffuse * specVal;
			lightDiff += light[i].diffuse * NdotL + light[i].ambient;
		}
	}
	fragmentColor = vec4(lightDiff * texCol.rgb + lightSpec, texCol.a);
}
