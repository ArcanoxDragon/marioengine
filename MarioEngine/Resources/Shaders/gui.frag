#version 150

in vec4 vPos, oVPos;
in vec2 vUV;

uniform sampler2D tex;
uniform vec4 tintColor = vec4(1.0, 1.0, 1.0, 1.0);

out vec4 fragmentColor;

void main()
{
	vec4 texCol = texture(tex, vUV);

	fragmentColor = texCol * tintColor;
}
