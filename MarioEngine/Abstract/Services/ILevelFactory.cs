﻿using MarioEngine.API;
using MarioEngine.Engine;

namespace MarioEngine.Abstract.Services
{
    public interface ILevelFactory
	{
		Level Create( IMarioGame game );
	}
}
