﻿using System;
using MarioEngine.API;

namespace MarioEngine.Abstract.Services
{
    public interface IContentProviderFactory
	{
		IContentProvider Create( Type contentProviderType );
	}
}
