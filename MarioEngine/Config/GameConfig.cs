﻿using ConfigLibrary;
using MarioEngine.API.Config;

namespace MarioEngine.Config
{
	class GameConfig : IGameConfig
	{
		public static GameConfig Instance { get; private set; }

		public static void LoadConfig()
		{
			Instance = new GameConfig();
		}

		#region Defaults

		private const bool DefaultSoundEnabled = true;
		private const bool DefaultMusicEnabled = true;

		#endregion

		private const string ConfigFolder = "Config";
		private const string ConfigFile   = "Game.cfg";

		private readonly ConfigFile configFile;

		#region Config Properties

		private bool isSoundEnabled;
		private bool isMusicEnabled;

		#endregion

		private GameConfig()
		{
			this.configFile = new ConfigFile( $"{ConfigFolder}/{ConfigFile}", this.BuildDefault );
			this.configFile.GetValue( "SoundEnabled", out this.isSoundEnabled );
			this.configFile.GetValue( "MusicEnabled", out this.isMusicEnabled );
		}

		private void BuildDefault( ConfigFile config )
		{
			config.RootNode.SetValue( "SoundEnabled", DefaultSoundEnabled );
			config.RootNode.SetValue( "MusicEnabled", DefaultMusicEnabled );
		}

		public bool IsSoundEnabled
		{
			get => this.isSoundEnabled;
			set
			{
				this.isSoundEnabled = value;
				this.configFile.RootNode.SetValue( "SoundEnabled", value );
				this.configFile.SaveConfig();
			}
		}

		public bool IsMusicEnabled
		{
			get => this.isMusicEnabled;
			set
			{
				this.isMusicEnabled = value;
				this.configFile.RootNode.SetValue( "MusicEnabled", value );
				this.configFile.SaveConfig();
			}
		}
	}
}