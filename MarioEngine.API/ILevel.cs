﻿using System;
using System.Collections.Generic;
using MarioEngine.API.Tiles;

namespace MarioEngine.API
{
	public interface ILevel
	{
		IMarioGame    Game       { get; }
		Random        Random     { get; }
		List<ITile>[] Layers     { get; }
		float           CameraX    { get; set; }
		float           CameraY    { get; set; }
		float         ViewWidth  { get; set; }
		float         ViewHeight { get; set; }

		ITile[] GetObjects( uint layer );
		void AddTile( uint layer, ITile tile );
		void RemoveTile( uint layer, ITile tile );
	}
}