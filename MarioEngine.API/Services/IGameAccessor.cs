﻿namespace MarioEngine.API.Services
{
	public interface IGameAccessor
	{
		IMarioGame Game { get; }
	}
}