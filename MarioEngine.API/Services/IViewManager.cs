﻿namespace MarioEngine.API.Services
{
	public interface IViewManager
	{
		void SetProjectionForGui();
		void SetProjectionForLevel( float cameraX, float cameraY, float viewHeight );
	}
}