﻿namespace MarioEngine.API.Services.Tiles
{
    public interface ITileHandlerFactory
	{
		THandler Create<THandler>() where THandler : ITileHandler;
	}
}
