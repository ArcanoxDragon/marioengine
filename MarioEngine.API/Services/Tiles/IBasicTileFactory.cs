﻿using System;
using MarioEngine.API.Tiles;

namespace MarioEngine.API.Services.Tiles
{
	public interface IBasicTileFactory
	{
		ITile CreateTile( Type tileType, string tileTypeName );
		TTile CreateTile<TTile>( string tileTypeName ) where TTile : ITile;
	}
}