﻿using System.IO;
using MarioEngine.API.Tiles;

namespace MarioEngine.API.Services.Tiles
{
	public interface ITileReader
	{
		ITile ReadTile( BinaryReader reader, string tileTypeName );
	}
}