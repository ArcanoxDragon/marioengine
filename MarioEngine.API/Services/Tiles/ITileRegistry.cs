﻿using System.IO;
using MarioEngine.API.Tiles;

namespace MarioEngine.API.Services.Tiles
{
	public interface ITileRegistry
	{
		void RegisterTileHandler<THandler>( THandler handler )
			where THandler : ITileHandler;

		void RegisterTileHandler<THandler>()
			where THandler : ITileHandler;

		bool CanHandleType( string typeName );
		ITileHandler GetHandlerFor( string typeName );
		ITile ReadTile( BinaryReader reader, string typeName );
		void WriteTile( BinaryWriter writer, ITile tile );
	}
}