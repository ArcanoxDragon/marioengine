﻿using System.IO;
using MarioEngine.API.Tiles;

namespace MarioEngine.API.Services.Tiles
{
	public interface ITileWriter
	{
		void WriteTile( BinaryWriter writer, ITile tile );
	}
}