﻿namespace MarioEngine.API.Services.Tiles
{
	public interface ITileHandler : ITileReader, ITileWriter
	{
		bool HandlesType( string typeName );
	}
}