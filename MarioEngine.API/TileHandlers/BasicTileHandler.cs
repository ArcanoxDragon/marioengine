﻿using System;
using System.Collections.Generic;
using System.IO;
using GameDotNet.Extensions;
using MarioEngine.API.Services.Tiles;
using MarioEngine.API.Tiles;

namespace MarioEngine.API.TileHandlers
{
	public sealed class BasicTileHandler : ITileHandler
	{
		private readonly Dictionary<string, Type> tileClasses;
		private readonly IBasicTileFactory        tileFactory;

		public BasicTileHandler( IBasicTileFactory tileFactory )
		{
			this.tileClasses = new Dictionary<string, Type>();
			this.tileFactory = tileFactory;
		}

		public void AddType<T>( string typeName ) where T : ITile
		{
			this.tileClasses.Add( typeName, typeof( T ) );
		}

		public bool HandlesType( string typeName )
			=> this.tileClasses.ContainsKey( typeName );

		public ITile ReadTile( BinaryReader reader, string tileTypeName )
		{
			if ( reader.BaseStream.GetAvailable() < 2 * sizeof( int ) ) // X and Y
				throw new IOException( $"Not enough data to read tile of type \"{tileTypeName}\"" );

			var x = reader.ReadInt32();
			var y = reader.ReadInt32();

			ITile returnTile;

			if ( this.tileClasses.ContainsKey( tileTypeName ) )
			{
				var tileType = this.tileClasses[ tileTypeName ];

				returnTile = this.tileFactory.CreateTile( tileType, tileTypeName );
			}
			else
			{
				returnTile = this.tileFactory.CreateTile<BasicTile>( tileTypeName );
			}

			returnTile.X = x;
			returnTile.Y = y;

			return returnTile;
		}

		public void WriteTile( BinaryWriter writer, ITile tile )
		{
			if ( !this.tileClasses.ContainsKey( tile.Type ) && tile.GetType() != typeof( BasicTile ) )
				throw new InvalidOperationException( $"{nameof(BasicTileHandler)} cannot handle tile of type \"{tile.GetType().FullName}\"" );

			writer.Write( tile.X );
			writer.Write( tile.Y );
		}
	}
}