﻿using System;
using GameDotNet.Abstract.Services.Graphics;

namespace MarioEngine.API.Tiles
{
	public abstract class TileBase : ITile
	{
		protected TileBase( string tileTypeName )
		{
			this.Type = tileTypeName ?? throw new ArgumentNullException( nameof(tileTypeName), "Tile type name cannot be null" );
		}

		public string Type  { get; }
		public int    X     { get; set; }
		public int    Y     { get; set; }
		public ILevel Level { get; private set; }
		public uint   Layer { get; private set; }

		public IMarioGame        Game      => this.Level?.Game;
		public ITransformManager Transform => this.Game?.Transform;

		public abstract int Width  { get; }
		public abstract int Height { get; }

		public void AddToLevel( ILevel level, uint layer )
		{
			this.Level = level ?? throw new ArgumentNullException( nameof(level), "Cannot add tile to null level!" );
			this.Layer = layer;
			this.OnAddedToLevel();
		}

		public void RemoveFromLevel()
		{
			this.OnRemovingFromLevel();
			this.Level = null;
		}

		public virtual void Update( TimeSpan deltaTime ) { }
		public virtual void Render( TimeSpan deltaTime ) { }

		protected virtual void OnAddedToLevel() { }
		protected virtual void OnRemovingFromLevel() { }
	}
}