﻿namespace MarioEngine.API.Tiles
{
	public sealed class BasicTile : TileBase
	{
		public BasicTile( string tileTypeName ) : base( tileTypeName ) { }

		public override int Width  => 1;
		public override int Height => 1;
	}
}