﻿using System;

namespace MarioEngine.API.Tiles
{
	public interface ITile
	{
		int    X      { get; set; }
		int    Y      { get; set; }
		int    Width  { get; }
		int    Height { get; }
		string Type   { get; }
		ILevel Level  { get; }
		uint   Layer  { get; }

		void AddToLevel( ILevel level, uint layer );
		void RemoveFromLevel();
		void Update( TimeSpan deltaTime );
		void Render( TimeSpan deltaTime );
	}
}