﻿using System;
using GameDotNet.Abstract.Engine;
using GameDotNet.Abstract.Services.Graphics;
using GameDotNet.Audio;
using OpenTK.Input;

namespace MarioEngine.API
{
	public abstract class MarioState : State<IMarioGame, MarioState>
	{
		protected MarioState( IMarioGame game ) : base( game ) { }

		public abstract string Name { get; }

		public virtual string SoundtrackName => default;

		protected ITransformManager Transform => this.Game.Transform;

		public SoundSource EffectSource { get; set; }
		public SoundSource MusicSource  { get; set; }
		public Soundtrack  Music        { get; set; }

		protected override void OnLeaving()
		{
			base.OnLeaving();

			this.Music?.Stop();
		}

		public override void Update( TimeSpan deltaTime )
		{
			this.InitializeTickResourceIfNeeded();

			if ( this.Game.GetKeyOneShot( Key.F5, true ) )
				this.UnloadAllResources();
		}

		public override void Render( TimeSpan deltaTime )
		{
			this.InitializeRenderResourcesIfNeeded();
		}

		protected virtual void UnloadAllResources()
		{
			this.Game.Animations.UnloadAll();
			this.Game.Fonts.UnloadAll();
			this.Game.Models.UnloadAll();
			this.Game.Resources.UnloadAll();
			this.Game.Shaders.UnloadAll();
			this.Game.Sounds.UnloadAll();
			this.Game.Soundtracks.UnloadAll();
			this.Game.Textures.UnloadAll();

			this.Music?.Stop();
			this.Music = null;
		}

		protected virtual void InitializeTickResourceIfNeeded()
		{
			if ( this.EffectSource == default )
				this.EffectSource = new SoundSource { Volume = this.Game.Config.IsSoundEnabled ? 1.0f : 0.0f };

			if ( this.MusicSource == default )
				this.MusicSource = new SoundSource();

			if ( this.Music == default && this.SoundtrackName != default )
			{
				this.MusicSource.Volume = this.Game.Config.IsMusicEnabled ? 1.0f : 0.0f;

				this.Music = this.Game.Soundtracks.Get( this.SoundtrackName );
				this.Music.Play( this.MusicSource );
			}
		}

		protected virtual void InitializeRenderResourcesIfNeeded() { }
	}
}