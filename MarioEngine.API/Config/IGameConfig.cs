﻿namespace MarioEngine.API.Config
{
	public interface IGameConfig
	{
		bool IsSoundEnabled { get; set; }
		bool IsMusicEnabled { get; set; }
	}
}