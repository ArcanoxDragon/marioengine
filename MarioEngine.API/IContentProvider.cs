﻿using Ninject;

namespace MarioEngine.API
{
	public interface IContentProvider
	{
		string Name { get; }

		void RegisterModules( IKernel kernel );
		void LoadContent();
	}
}