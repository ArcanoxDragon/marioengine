﻿using System;

namespace MarioEngine.API.Attributes
{
	[ AttributeUsage( AttributeTargets.Class ) ]
	public class PluginAttribute : Attribute { }
}