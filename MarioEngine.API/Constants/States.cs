﻿namespace MarioEngine.API.Constants
{
	public class StateNames
	{
		public const string Menu   = "Menu";
		public const string Editor = "Editor";
		public const string Level  = "Level";
	}
}