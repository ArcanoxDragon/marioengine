﻿using System.Drawing;
using OpenTK.Graphics;

namespace MarioEngine.API.Constants
{
	public static class GuiConstants
	{
		public const int ControlSpacing = 8;

		public static class Buttons
		{
			public static readonly Size ButtonSize = new Size( 256, 48 );

			public static readonly Color4 NormalTintColor   = new Color4( 250, 250, 250, 255 );
			public static readonly Color4 SelectedTintColor = new Color4( 255, 200, 63,  255 );
		}
	}
}