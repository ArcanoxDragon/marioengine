﻿namespace MarioEngine.API.Constants
{
	public class LevelConstants
	{
		public const int CurrentBinaryFormatVersion = 1;
		public const int CurrentJsonFormatVersion   = 1;
	}
}