﻿using GameDotNet.Abstract.Engine;
using MarioEngine.API.Config;
using MarioEngine.API.Services;
using MarioEngine.API.Services.Tiles;
using OpenTK.Input;

namespace MarioEngine.API
{
	public interface IMarioGame : IGameWithStates<IMarioGame, MarioState>
	{
		string        Title             { get; set; }
		ITileRegistry TileRegistry      { get; }
		IViewManager  ViewManager       { get; }
		KeyboardState KeyboardState     { get; }
		MouseState    MouseState        { get; }
		KeyboardState LastKeyboardState { get; }
		MouseState    LastMouseState    { get; }
		IGameConfig   Config            { get; }

		bool GetKeyOneShot( Key key, bool onRelease = false );
		bool GetMouseOneShot( MouseButton button, bool onRelease = false );
	}
}